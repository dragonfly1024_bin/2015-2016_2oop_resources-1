#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
	
"""


import os
import sys


class Compte:
	
	def __init__(self, nom, identifiant, solde):				# Constructeur de la classe Compte.
		self.nom = nom
		self.identifiant = identifiant
		self.solde = solde
		
	def afficherSolde(self):
		print("\nLe solde vaut : %f" % self.solde)


def main():
	
	mon_compte = Compte("Dupont", 12324, 100.50)				# Création d'un objet de la classe compte. On fait alors appelle au constructeur.
	mon_compte.afficherSolde()									# Affichage du solde
	
	return 0


if __name__ == "__main__":
	main()
