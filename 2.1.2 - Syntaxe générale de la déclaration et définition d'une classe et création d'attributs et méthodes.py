#!/usr/bin/python
# -*- coding: utf-8 -*-


"""
	
"""


import os
import sys


class Compte:

	def crediter(self, x):						# Déclaration et définition d'une méthode de l'objet.
		self.solde += x
		
	def afficherSolde(self):
		print("\nLe solde vaut: %f" % self.solde)


def main():
	
	mon_compte = Compte()						# Déclaration d'un objet de la classe Compte
	
	mon_compte.nom = "Dupont"					# Déclaration et définition d'un attribut de l'objet.
	mon_compte.identifiant = 130789
	mon_compte.solde = 100.5
	
	mon_compte.afficherSolde()					# Appelle à une méthode de l'objet.
	
	return 0


if __name__ == "__main__":
	main()
